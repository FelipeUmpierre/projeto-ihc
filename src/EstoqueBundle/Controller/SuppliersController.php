<?php

namespace EstoqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class SuppliersController
 * @package EstoqueBundle\Controller
 * @Route("/fornecedores")
 */
class SuppliersController extends Controller
{
	/**
	 * @Route("/", name="_suppliers")
	 * @Template()
	 */
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/new", name="_suppliers_new")
	 * @Template()
	 */
	public function newAction()
	{
		return array();
	}
}
