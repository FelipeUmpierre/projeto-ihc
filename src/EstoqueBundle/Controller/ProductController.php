<?php

namespace EstoqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class ProductsController
 * @package EstoqueBundle\Controller
 * @Route("/produtos")
 */
class ProductController extends Controller
{
	/**
	 * @Route("/", name="_products")
	 * @Template()
	 */
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/edit", name="_products_edit")
	 * @Template()
	 */
	public function editAction()
	{
		return array();
	}

	/**
	 * @Route("/new", name="_products_new")
	 * @Template()
	 */
	public function newAction()
	{
		return array();
	}

}
