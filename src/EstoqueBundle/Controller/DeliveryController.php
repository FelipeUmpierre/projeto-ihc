<?php

namespace EstoqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DeliveryController
 * @package EstoqueBundle\Controller
 * @Route("/entregas")
 */
class DeliveryController extends Controller
{
	/**
	 * @Route("/index", name="_delivery")
	 * @Template()
	 */
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/new", name="_delivery_new")
	 * @Template()
	 */
	public function newAction()
	{
		return array();
	}
}
