<?php

namespace EstoqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class RequestsController
 * @package EstoqueBundle\Controller
 * @Route("/pedidos")
 */
class RequestsController extends Controller
{
	/**
	 * @Route("/index", name="_requests")
	 * @Template()
	 */
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/new", name="_requests_new")
	 * @Template()
	 */
	public function newAction()
	{
		return array();
	}
}
