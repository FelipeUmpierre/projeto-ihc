<?php

namespace EstoqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class FinancialController
 * @package EstoqueBundle\Controller
 * @Route("/financeiro")
 */
class FinancialController extends Controller
{
	/**
	 * @Route("/", name="_financial")
	 * @Template()
	 */
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/new", name="_financial_new")
	 * @Template()
	 */
	public function newAction()
	{
		return array();
	}
}
